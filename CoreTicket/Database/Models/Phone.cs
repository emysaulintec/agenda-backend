﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Models
{
    public class Phone : Entity
    {
        public string Number { get; set; }
        public PhoneType PhoneType { get; set; }
        public bool IsWhatsap { get; set; }
        public int ContactId { get; set; }

        [ForeignKey(nameof(ContactId))]
        public virtual Contact Contact { get; set; }
    }
}
