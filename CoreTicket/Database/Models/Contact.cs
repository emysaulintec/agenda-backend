﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Models
{
    public class Contact : Entity
    {
        public Contact()
        {
            this.Phones = new HashSet<Phone>();
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PictureUrl { get; set; }
        public int UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        public virtual ICollection<Phone> Phones { get; set; }

    }
}
