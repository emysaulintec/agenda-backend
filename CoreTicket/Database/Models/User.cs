﻿using System.Collections.Generic;

namespace Database.Models
{
    public class User : Entity
    {
        public User()
        {
            this.Contacts = new HashSet<Contact>();
        }

        public string UserName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }

    }
}
