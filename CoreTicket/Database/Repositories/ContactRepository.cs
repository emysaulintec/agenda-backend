﻿using Database.Interfaces;
using Database.Models;

namespace Database.Repositories
{
    public class ContactRepository : RepositoryBase<Contact>, IContactRepository
    {
        public ContactRepository(ContactsContext ticketContext) : base(ticketContext)
        {
        }
    }
}
