﻿using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Database.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : Entity
    {
        protected ContactsContext TicketContext { get; set; }
        public RepositoryBase(ContactsContext ticketContext)
        {
            this.TicketContext = ticketContext;
        }
        public IQueryable<T> FindAll(Expression<Func<T, object>> include = null)
        {
            return include == null ? this.TicketContext.Set<T>().AsNoTracking()
            : this.TicketContext.Set<T>().AsNoTracking().Include(include);
        }
        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.TicketContext.Set<T>().Where(expression).AsNoTracking();
        }
        public T Create(T entity)
        {
            var r = this.TicketContext.Set<T>().Add(entity);
            this.TicketContext.SaveChanges();
            return r.Entity;
        }
        public void Update(T entity)
        {
            this.TicketContext.Set<T>().Update(entity);
            this.TicketContext.SaveChanges();

        }
        public void Delete(int entityId)
        {
            T entity = this.TicketContext.Set<T>().FirstOrDefault(x => x.Id == entityId);
            this.TicketContext.Set<T>().Remove(entity);
            this.TicketContext.SaveChanges();
        }
    }
}
