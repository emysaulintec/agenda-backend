﻿using Database.Interfaces;
using Database.Models;

namespace Database.Repositories
{
    public class PhoneRepository : RepositoryBase<Phone>, IPhoneRepository
    {
        public PhoneRepository(ContactsContext ticketContext) : base(ticketContext)
        {
        }
    }
}
