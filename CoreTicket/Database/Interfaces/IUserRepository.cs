﻿using Database.Models;

namespace Database.Interfaces
{
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
}
