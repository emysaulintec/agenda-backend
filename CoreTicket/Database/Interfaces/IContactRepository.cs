﻿
using Database.Models;

namespace Database.Interfaces
{
    public interface IContactRepository : IRepositoryBase<Contact>
    {
    }
}
