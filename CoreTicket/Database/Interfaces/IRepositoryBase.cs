﻿using Database.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Database.Interfaces
{
    public interface IRepositoryBase<T> where T: Entity
    {
        IQueryable<T> FindAll(Expression<Func<T, object>> include = null);
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        T Create(T entity);
        void Update(T entity);
        void Delete(int entityId);
    }
}
