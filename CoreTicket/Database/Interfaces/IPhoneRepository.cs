﻿using Database.Models;

namespace Database.Interfaces
{
    public interface IPhoneRepository : IRepositoryBase<Phone>
    {
    }
}
