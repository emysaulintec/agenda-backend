﻿using Database.Models;

namespace CoreTicket.Dtos.Phones
{
    public class PhoneDto : TEntityDto
    {
        public string Number { get; set; }
        public PhoneType PhoneType { get; set; }
        public bool IsWhatsap { get; set; }
        public int? ContactId { get; set; }
    }
}
