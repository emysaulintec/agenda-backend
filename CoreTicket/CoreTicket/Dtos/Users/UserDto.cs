﻿using CoreTicket.Dtos.Contacts;
using System.Collections.Generic;

namespace CoreTicket.Dtos.Users
{
    public class UserDto : TEntityDto
    {
        public UserDto()
        {
            this.Contacts = new List<ContactDto>();
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public List<ContactDto> Contacts { get; set; }
    }
}
