﻿namespace CoreTicket.Dtos.Users
{
    public class LoginResult
    {
        public UserDto User { get; set; }
        public string Token { get; set; }
    }
}
