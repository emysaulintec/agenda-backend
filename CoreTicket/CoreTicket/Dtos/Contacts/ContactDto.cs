﻿using CoreTicket.Dtos.Phones;
using System.Collections.Generic;

namespace CoreTicket.Dtos.Contacts
{
    public class ContactDto : TEntityDto
    {
        public ContactDto()
        {
            Phones = new List<PhoneDto>();
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string PictureUrl { get; set; }
        public int UserId { get; set; }

        public List<PhoneDto> Phones { get; set; }
    }
}
