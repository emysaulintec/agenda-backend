﻿using AutoMapper;
using CoreTicket.Dtos.Contacts;
using CoreTicket.Dtos.Phones;
using CoreTicket.Dtos.Users;
using Database.Models;

namespace CoreTicket
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<User, UserCreateDto>();
            CreateMap<UserCreateDto, User>();

            CreateMap<Contact, ContactDto>();
            CreateMap<ContactDto, Contact>();

            CreateMap<Phone, PhoneDto>();
            CreateMap<PhoneDto, Phone>();
        }
    }
}