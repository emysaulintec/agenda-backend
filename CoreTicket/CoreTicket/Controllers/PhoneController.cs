﻿using AutoMapper;
using CoreTicket.Dtos.Phones;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneController : TBaseController<Phone, PhoneDto, PhoneDto, PhoneDto>
    {
        public PhoneController(IMapper mapper, IRepositoryBase<Phone> repositoryBase) : base(mapper, repositoryBase)
        {
        }
    }
}
