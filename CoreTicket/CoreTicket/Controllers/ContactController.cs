﻿using AutoMapper;
using CoreTicket.Dtos.Contacts;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : TBaseController<Contact, ContactDto, ContactDto, ContactDto>
    {
        private IMapper _mapper { get; set; }
        public ContactController(IMapper mapper, IRepositoryBase<Contact> repositoryBase) : base(mapper, repositoryBase)
        {
            this.Include = (x => x.Phones);
            this._mapper = mapper;
        }

        [HttpGet]
        [Route("by-users/{userId}")]
        public IEnumerable<ContactDto> GetAll(int userId)
        {
            var result = _repositoryBase.FindAll(Include).Where(x => x.UserId == userId).ToList();
            return _mapper.Map<List<ContactDto>>(result);
        }
    }
}
