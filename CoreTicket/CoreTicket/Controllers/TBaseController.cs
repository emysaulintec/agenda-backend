﻿using AutoMapper;
using CoreTicket.Dtos;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CoreTicket.Controllers
{
    public abstract class TBaseController<T, TGetDto, TCreateDto, TUpdateDto> : ControllerBase where T : Entity where TGetDto : class where TUpdateDto : class where TCreateDto : class
    {
        private readonly IMapper _mapper;

        public IRepositoryBase<T> _repositoryBase { get; set; }
        public TBaseController(IMapper mapper, IRepositoryBase<T> repositoryBase)
        {
            this._repositoryBase = repositoryBase;
            this._mapper = mapper;
        }

        public Expression<Func<T, object>> Include { get; set; }


        [HttpGet]
        public virtual IEnumerable<TGetDto> GetAll()
        {
            var result = _repositoryBase.FindAll(Include).ToList();
            return _mapper.Map<List<TGetDto>>(result);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual TGetDto Get(int id)
        {
            var result = _repositoryBase.FindAll(Include).FirstOrDefault(x => x.Id == id);
            return _mapper.Map<TGetDto>(result);
        }

        [HttpPut]
        [Route("{id}")]
        public virtual void Update(int id, TUpdateDto ticketToUpdate)
        {
            var ticket = _repositoryBase.FindAll().FirstOrDefault(x => x.Id == id);
            ticket = _mapper.Map<T>(ticketToUpdate);
            ticket.Id = id;
            ticket.UpdatedTime = DateTime.Now;
            _repositoryBase.Update(ticket);
        }
        [HttpPost]
        public virtual long Create([FromBody] TCreateDto currentTicket)
        {
            var entity = _mapper.Map<T>(currentTicket);
            entity.CreationTime = System.DateTime.Now;
            return _repositoryBase.Create(entity).Id;
        }

        [HttpDelete]
        [Route("{id}")]
        public virtual void Delete(int id)
        {
            _repositoryBase.Delete(id);
        }
    }
}
