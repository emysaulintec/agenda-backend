﻿using AutoMapper;
using CoreTicket.Dtos.Users;
using Database.Interfaces;
using Database.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;

namespace CoreTicket.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : TBaseController<User, UserDto, UserCreateDto, UserCreateDto>
    {
        public IMapper _mapper { get; set; }
        public UserController(IMapper mapper, IRepositoryBase<User> repositoryBase) : base(mapper, repositoryBase)
        {
            this._mapper = mapper;
        }



        [HttpPost]
        [Route("login")]
        public ActionResult<LoginResult> Login(LoginDto loginDto)
        {
            var user = _repositoryBase.FindAll()
                              .FirstOrDefault(x => x.UserName.ToLower() == loginDto.UserName && x.Password == loginDto.Password);
            if (user != null)
                return new LoginResult()
                {
                    User = _mapper.Map<UserDto>(user),
                    Token = "566546546"
                };

            return NotFound();
        }
    }
}
